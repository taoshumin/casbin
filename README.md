#### 匹配规则修改

支持匹配路由  /depts/:id
```cassandraql
keyMatch2	a URL path like /alice_data/resource1	a URL path or a : pattern like /alice_data/:resource	keymatch2_model.conf/keymatch2_policy.csv

修改conf/model.conf
原来:
r.obj = p.obj
修改为:
keyMatch2(r.obj,p.obj)
```

正则匹配

支持路由规则   /depts:id   GET|POST
```cassandraql
修改conf/model.conf
原来:
r.act == p.act

修改为:
regexMatch(r.act,p.act)
```