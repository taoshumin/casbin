package lib

import (
	"github.com/casbin/casbin/v2"
	"github.com/casbin/gorm-adapter/v3"
	"log"
)

var E *casbin.Enforcer

func init() {
	initDB()
	adapter,err:=gormadapter.NewAdapterByDB(DB)
	if err!=nil{
		log.Fatal(err)
	}
	e,err:=casbin.NewEnforcer("conf/model.conf",adapter)
	if err!=nil{
		log.Fatal(err)
	}
	err =e.LoadPolicy()
	if err!=nil{
		log.Fatal(err)
	}
	E = e
	initPolicy()
}

func initPolicy()  {

	m:=make([]*RoleRel,0)
	GetRoles(0,&m,"")
	// 初始化角色
	for _, r := range m {
		_,err:=E.AddRoleForUser(r.PRole,r.Role)
		if err!=nil{
			log.Fatal(err)
		}
	}

	// 初始化用户
	userRoles := GetUserRoles()
	for _, u := range userRoles {
		_,err:=E.AddRoleForUser(u.UserName,u.RoleName)
		if err!=nil{
			log.Fatal(err)
		}
	}

	//  初始化路由
	routerRoles := GetRouterRoles()
	for _, i2 := range routerRoles {
		_,err:=E.AddPolicy(i2.RoleName,i2.RouterUri,i2.RouterMethod)
		if err!=nil{
			log.Fatal(err)
		}
	}
}