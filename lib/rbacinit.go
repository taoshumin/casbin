package lib

import "gitlab.bj.flash.com/casbin/model"

type RoleRel struct {
	PRole string //  上个角色名
	Role  string // 本层级角色名
}

func (this *RoleRel) String() string {
	return this.PRole + ":" + this.Role
}

// 获取角色
func GetRoles(pid int, m *[]*RoleRel, pname string) {
	proles := make([]*model.Role, 0)
	DB.Where("role_pid=?", pid).Find(&proles)
	if len(proles) == 0 {
		return
	}
	for _, i2 := range proles {
		if pname == "" {
			*m = append(*m, &RoleRel{
				PRole: pname,
				Role:  i2.RoleName,
			})
		}
		GetRoles(i2.RoleId, m, i2.RoleName)
	}
}

// 获取用户和角色关系
func GetUserRoles() (users []*model.Users) {
	DB.Select("a.user_name,c.role_name").Table("users a,user_roles b,roles c").
		Where("a.user_id=b.user_id AND b.role_id=c.role_id").
		Order("a.user_id desc").Find(&users)
	return
}

// 获取路由与角色关系
func GetRouterRoles() (routers []*model.Routers) {
	DB.Select("a.r_uri,a.r_method,c.role_name").Table("routers a,router_roles b,roles c").
		Where("a.r_id=b.router_id and b.role_id=c.role_id").
		Order("role_name").Find(&routers)

	return
}
