package lib

import (
	"errors"
	"strings"
)

// 参考文献 https://casbin.org/docs/zh-CN/function
// Matchers函数
func init()  {
	E.AddFunction("methodMatch", func(arguments ...interface{}) (interface{}, error) {
		if len(arguments) == 2 {
			k1,k2:=arguments[0].(string),arguments[1].(string)
			return MethodMatch(k1,k2),nil
		}
		return nil,errors.New("methodMatch error")
	})
}

// [POST  GET ]能匹配到 中间空格
func MethodMatch(key1 string, key2 string) bool{
	s := strings.Split(key2," ")
	for _, i2 := range s {
		if i2 == key1 {
			return true
		}
	}
	return false
}