package model

import "fmt"

type Routers struct {
	RouterId     int    `gorm:"column:r_id;primaryKey"`
	RouterName   string `gorm:"column:r_name"`
	RouterUri    string `gorm:"column:r_uri;"`
	RouterMethod string `gorm:"column:r_method"`
	RouterPid    int    `gorm:"column:r_pid"`
	RouterStatus int    `gorm:"column:r_status"`
	RoleName     string
}

func (this *Routers) TableName() string {
	return "routers"
}
func (this *Routers) String() string {
	return fmt.Sprintf("%s-%s", this.RouterName, this.RoleName)
}
