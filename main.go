package main

import (
	"github.com/casbin/casbin/v2"
	"github.com/gin-gonic/gin"
	"gitlab.bj.flash.com/casbin/lib"
	"log"
)

func main() {
	r :=gin.New()

	r.Use(lib.Middleware()...)

	r.GET("/depts", func(context *gin.Context) {
		context.JSON(200,gin.H{"data":"部门列表"})
	})

	r.GET("/depts/:id", func(context *gin.Context) {
		context.JSON(200,gin.H{"data":"部门详情"})
	})

	r.POST("/depts", func(context *gin.Context) {
		context.JSON(200,gin.H{"data":"批量修改部门列表"})
	})

	r.Run(":8080")
}

func main02() {
	sub:= "shenyi" // 想要访问资源的用户。
	obj:= "/depts" // 将被访问的资源。
	act:= "GET" // 用户对资源执行的操作。
	e,_:= casbin.NewEnforcer("conf/model_t.conf","conf/p_t.csv")

	ok,err:= e.Enforce(sub,"domain1", obj, act)
	if err==nil && ok {
		log.Println("运行通过")
	}
}
